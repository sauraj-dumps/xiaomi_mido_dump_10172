## mido-user 7.0 NRD90M V9.6.1.0.NCFMIFD release-keys
- Manufacturer: xiaomi
- Platform: msm8953
- Codename: mido
- Brand: Xiaomi
- Flavor: lineage_mido-userdebug
- Release Version: 12
- Id: SP2A.220505.002
- Incremental: 1652694976
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: xiaomi/mido/mido:7.0/NRD90M/V9.6.1.0.NCFMIFD:user/release-keys
- OTA version: 
- Branch: mido-user-7.0-NRD90M-V9.6.1.0.NCFMIFD-release-keys
- Repo: xiaomi_mido_dump_10172


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
